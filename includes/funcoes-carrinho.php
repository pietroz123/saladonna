<?php

// Includes
include $_SERVER['DOCUMENT_ROOT'].'/config/sessao.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/includes/logica-produtos.php';

/*
 * Adiciona um item ao carrinho, dado seu id e quantidades
*/
function adicionar_carrinho($conexao, $id, $qtd_300, $qtd_500) {

    // Busca as informações da salada
    $salada = buscar_salada($conexao, $id);

    // Verifica se a sessão do carrinho existe
    if(isset($_SESSION['carrinho'])) {
        $contador = count($_SESSION['carrinho']);

        $ids_produtos = array_column($_SESSION['carrinho'], 'id');

        // Verifica se o produto que estamos adicionando já não está no carrinho
        if(!in_array($id, $ids_produtos)) {

            $_SESSION['carrinho'][$contador] = array (
                'id' => $id,
                'nome' => $salada['nome'],
                'imagem' => $salada['imagem'],
                'qtd_300' => $qtd_300,
                'qtd_500' => $qtd_500
            );
        }
        else {
            // Se o produto já existe, incrementamos a quantidade dele no carrinho
            for($i = 0; $i < count($ids_produtos); $i++){
                if($ids_produtos[$i] == $id){
                    // Incrementa a quantidade
                    $_SESSION['carrinho'][$i]['qtd_300'] += $qtd_300;
                    $_SESSION['carrinho'][$i]['qtd_500'] += $qtd_500;
                }
            }

        }

    }
    else {
        // Cria a sessão do carrinho
        $_SESSION['carrinho'][0] = array (
            'id' => $id,
            'nome' => $salada['nome'],
            'imagem' => $salada['imagem'],
            'qtd_300' => $qtd_300,
            'qtd_500' => $qtd_500
        );

    }
}

/*
 * Retorna o preço total do carrinho
*/
function preco_total() {

    $total = 0;

    foreach ($_SESSION['carrinho'] as $item_carrinho) {

        $total += $item_carrinho['qtd_300'] * PRECO_300;
        $total += $item_carrinho['qtd_500'] * PRECO_500;
        
    }

    return $total;

}

/*
 * Retorna o número de itens no carrinho
*/
function num_itens() {

    return count($_SESSION['carrinho']);

}

/*
 * Retorna o preço de um item no carrinho
*/
function preco_item($item_carrinho) {
    
    return $item_carrinho['qtd_300'] * PRECO_300 + $item_carrinho['qtd_500'] * PRECO_500;

}

/*
 * Retorna a quantidade de um item no carrinho
*/
function qtd_item($item_carrinho) {

    return $item_carrinho['qtd_300'] + $item_carrinho['qtd_500'];

}

/*
 * Retorna as observações do item no carrinho
*/
function observacoes_item($item_carrinho) {

    return '300ml: '.$item_carrinho['qtd_300'].' 500ml: '.$item_carrinho['qtd_500'];

}