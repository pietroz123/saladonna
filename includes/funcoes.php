<?php

function listar_query($conexao, $query) {
    $lista = array();
    $resultado = mysqli_query($conexao, $query);
    while ($item = mysqli_fetch_assoc($resultado))
        array_push($lista, $item);
    return $lista;
}

function buscar_salada($conexao, $id_salada) {
    $query = "SELECT * FROM saladas WHERE id = {$id_salada};";
    $resultado = mysqli_query($conexao, $query);
    $salada = mysqli_fetch_assoc($resultado);
    return $salada;
}

function buscar_ingrediente($conexao, $id_ingrediente) {
    $query = "SELECT * FROM ingredientes WHERE id = {$id_ingrediente};";
    $resultado = mysqli_query($conexao, $query);
    $ingrediente = mysqli_fetch_assoc($resultado);
    return $ingrediente;
}