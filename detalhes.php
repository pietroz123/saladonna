<?php
    require_once $_SERVER['DOCUMENT_ROOT'].'/database/conexao.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/includes/funcoes.php';
?>

<?php
    if (isset($_POST['id_salada'])) {
        $id = $_POST['id_salada'];
        $salada = buscar_salada($conexao, $id);
?>

<div class="modal-content">
    <div class="modal-header">
        <h2 id="titulo-salada"><?= $salada['nome'] ?></h2>
        <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
    </div>
    <div class="modal-body detalhes">
        <h3 id="titulo-ingredientes">Ingredientes</h3><br>
        <div class="ingredientes-salada">
            
        <?php
            $ingredientes = listar_query($conexao, "SELECT * FROM salada_ingredientes WHERE id_salada = {$id};");
            foreach ($ingredientes as $ingrediente) {
                $ingrediente_correspondente = buscar_ingrediente($conexao, $ingrediente['id_ingrediente']); 
        ?>
                <p class="ingrediente">
                    <img src="img/ingredientes/<?= $ingrediente_correspondente['imagem'] ?>" alt="<?= $ingrediente_correspondente['nome'] ?>">
                    <?= $ingrediente_correspondente['nome'] ?>
                </p>
        <?php
                }
        ?>
        
        </div>
        <div class="descricao-salada">
            <h3 id="titulo-descricao">Descrição</h3>
            <p class="descricao"><?= $salada['descricao'] ?></p>
        </div>
    </div>
    <div class="modal-footer" style="display: inline;">

        <form action="lista-produtos.php" method="post" id="form-adicionar">

            <div class="d-flex flex-row justify-content-center align-content-center" id="inputs-qtd">
                <div class="300 px-3">
                    <p class="font-weight-bold">300ml</p>
                    <div class="def-number-input number-input">
                        <button type="button" onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus"></button>
                        <input class="quantity" min="0" name="quantidade-300" value="0" type="number" id="input-qtd-300">
                        <button type="button" onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus"></button>
                    </div>
                </div>
                <div class="500 px-3">
                    <p class="font-weight-bold">500ml</p>
                    <div class="def-number-input number-input">
                        <button type="button" onclick="this.parentNode.querySelector('input[type=number]').stepDown()" class="minus"></button>
                        <input class="quantity" min="0" name="quantidade-500" value="0" type="number" id="input-qtd-500">
                        <button type="button" onclick="this.parentNode.querySelector('input[type=number]').stepUp()" class="plus"></button>
                    </div>
                </div>
            </div>
            <div class="erros">
                <!-- Preenchido caso qtd = 0 -->
            </div>

            <hr>
            <input type="hidden" name="id" value="<?= $salada['id'] ?>" id="input-id-salada">
            
            <button type="submit" name="submit-adicionar-carrinho" class="btn btn-secondary" id="btn-adicionar-carrinho">Adicionar ao carrinho</button>
            <button type="button" class="btn btn-danger btn-fechar" data-dismiss="modal">fechar</button>

        </form>
    </div>
</div>

<script>

    $(document).on('click', '#btn-adicionar-carrinho', function() {



    });

    $('#form-adicionar').submit(function(e) {
        e.preventDefault();

        // Recupera o ID
        var id_salada = $(this).children('#input-id-salada').val();
        
        // Recupera as quantidades
        var qtd_300 = $('#input-qtd-300').val();
        var qtd_500 = $('#input-qtd-500').val();

        // Verifica as quantidades e mostra erro caso 0
        if (qtd_300 == 0 && qtd_500 == 0) {
            $('.erros').append('<p class="text-danger">Por favor selecione uma quantidade</p>');
            $('.number-input').addClass('input-qtd-invalido');
            return;
        }
        
        
        $.ajax({
            url: 'scripts/adiciona-carrinho.php',
            method: 'post',
            data: {
                adicionar: "sim",
                id: id_salada,
                qtd_300: qtd_300,
                qtd_500: qtd_500
            },
            success: function(retorno) {

                // Atualiza o carrinho
                $('#nav-item-carrinho').html(retorno);

                // Mostra mensagem de sucesso
                toastr["success"]("<a href='#' onclick='abrirCarrinho();'>Visualizar carrinho</a>", "Item adicionado ao carrinho!");

                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }

                // Fecha o modal
                $('#modal-detalhes-salada').modal('hide');
            },
            error: function(retorno) {
                console.log('Error');
                console.log(retorno);
            }
        });

    });



</script>

<?php
    }