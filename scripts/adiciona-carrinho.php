<?php
    // Includes
    require_once $_SERVER['DOCUMENT_ROOT'].'/config/sessao.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/database/conexao.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/includes/funcoes.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/includes/funcoes-carrinho.php';

    if ( isset($_POST['adicionar']) && $_POST['adicionar'] == "sim") {


        $qtd_300 = $_POST['qtd_300'];
        $qtd_500 = $_POST['qtd_500'];

        adicionar_carrinho($conexao, $_POST['id'], $qtd_300, $qtd_500);


    }

    if (isset($_SESSION['carrinho'])) {
?>

<!-- Atualiza o HTML do carrinho -->

<?php
    $qtd_total = num_itens();
?>

<a href="#" class="nav-link" id="cart">

    <i class="fa fa-shopping-cart text-warning"></i>
    <span class="badge badge-light"><?= $qtd_total; ?></span>

</a>
<div class="shopping-cart">

    <div class="shopping-cart-header">
        <i class="fa fa-shopping-cart cart-icon text-warning"></i><span class="badge badge-light"><?= $qtd_total; ?></span>
        <div class="shopping-cart-total">
            <span class="lighter-text">Total:</span>
            <span class="main-color-text total">R$<?= preco_total(); ?></span>
        </div>
    </div>
    <!--end shopping-cart-header -->
    
    <ul class="shopping-cart-items">
    
    <?php
        foreach ($_SESSION['carrinho'] as $item_carrinho) {
    ?>
    
        <li class="clearfix">
            <img src="../img/saladas/<?= $item_carrinho['imagem'] ?>"/>
            <span class="item-name"><?= $item_carrinho['nome'] ?></span>
            <span class="item-detail"><div class="font-small"><?= observacoes_item($item_carrinho); ?></div></span>
            <span class="item-price">R$<?= preco_item($item_carrinho); ?></span>
            <span class="item-quantity">Quantidade: <?= qtd_item($item_carrinho); ?></span>
        </li>
    
    <?php
        }
    ?>
    
    </ul>
    
    <hr>
    <a href="carrinho.php" class="btn warning-color-dark botao btn-block float-right botao-pequeno">Visualizar carrinho <i class="fa fa-chevron-right"></i></a>
    <a href="checkout.php" class="btn warning-color botao btn-block float-right botao-pequeno">Finalizar compra <i class="fa fa-chevron-right"></i></a>

</div>
<!--end shopping-cart -->



<?php
    }
    else {
?>
        <a href="#" class="nav-link" id="cart">

            <i class="fa fa-shopping-cart text-warning"></i>
            <span class="badge badge-light">0</span>

        </a>
        <div class="shopping-cart">

            <div class="shopping-cart-header">
                <i class="fa fa-shopping-cart cart-icon text-warning"></i><span class="badge badge-light">0</span>
                <div class="shopping-cart-total">
                    <span class="lighter-text">Total:</span>
                    <span class="main-color-text total">R$0</span>
                </div>
            </div>
            <!--end shopping-cart-header -->

            <ul class="shopping-cart-items">

                <div class="alert alert-info">Não existem itens no carrinho.</div>

            </ul>

        </div>
        <!--end shopping-cart -->
<?php
    }
?>
