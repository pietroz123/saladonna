

$(document).ready(function() {

    // Preenche o modal detalhes da salada com AJAX
    $(".btn-detalhes").click(function() {
        var id_salada = $(this).attr("id");
        
        $.ajax({
            url: "detalhes.php",
            method: "post",
            data: {
                id_salada: id_salada
            },
            success: function(data) {
                $("#detalhes-salada").html(data);
                $("#modal-detalhes-salada").modal("show");
            }
        });

    });

    
});


// Funções para aumentar e diminuir a quantidade de itens
function aumentaQuantidade() {
    document.getElementById("input-quantidade").stepUp(1);
}
function diminuiQuantidade() {
    document.getElementById("input-quantidade").stepDown(1);
}

