
// =======================================================
// Ao clicar no ícone do carrinho
// =======================================================

$(document).on('click', '#cart', function(e) {

    e.stopPropagation();

    abrirCarrinho();
});


// Função para abrir o carrinho
function abrirCarrinho() {
    var delay = $('.shopping-cart').index() * 50 + 'ms';
    $('.shopping-cart').css({
        '-webkit-transition-delay': delay,
        '-moz-transition-delay': delay,
        '-o-transition-delay': delay,
        'transition-delay': delay
    });
    $(".shopping-cart").toggleClass("active");
}
