            </div>
        </main>

        <div id="footer">
            <footer class="page-footer blue-grey lighten-5">

                <div id="redes-sociais">
                    <div class="container">
                        <div class="row py-4 d-flex align-items-center">

                            <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">
                                <h6 class="mb-0">Conecte-se conosco através de nossas redes sociais!</h6>
                            </div>

                            <div class="col-md-6 col-lg-7 text-center text-md-right icones-redes">
                                <!-- Facebook -->
                                <a href="https://www.facebook.com/saladonnasaladanopote/">
                                    <img src="img/redes/facebook.png" alt="Facebook">
                                </a>
                                <!--Instagram-->
                                <a href="https://www.instagram.com/saladonna_delivery/">
                                    <img src="img/redes/instagram.png" alt="Instagram">
                                </a>
                                <!-- Whatsapp -->
                                <a href="https://wa.me/5515981319363">
                                    <img src="img/redes/whatsapp.png" alt="Whatsapp">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Footer Links -->
                <div class="container text-center text-md-left mt-5">

                    <!-- Grid row -->
                    <div class="row mt-3 dark-grey-text">

                        <!-- Grid column -->
                        <div class="col-md-3 col-lg-4 col-xl-3 mb-4">

                            <!-- Content -->
                            <h6 class="text-uppercase font-weight-bold">Saladonna</h6>
                            <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 100px;">
                            <p>Reeduque sua alimentação, perca peso e tenha uma vida mais saudável!</p>

                        </div>
                        <!-- Grid column -->

                        <!-- Grid column -->
                        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

                            <!-- Links -->
                            <h6 class="text-uppercase font-weight-bold">Produtos</h6>
                            <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 100px;">
                            <p>
                                <a class="dark-grey-text" href="#">Saladas</a>
                            </p>
                            <p>
                                <a class="dark-grey-text" href="#">Sucos</a>
                            </p>
                            <p>
                                <a class="dark-grey-text" href="#">Geladinhos</a>
                            </p>
                            <p>
                                <a class="dark-grey-text" href="#">Açaí</a>
                            </p>

                        </div>
                        <!-- Grid column -->

                        <!-- Grid column -->
                        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">

                            <!-- Links -->
                            <h6 class="text-uppercase font-weight-bold">Links</h6>
                            <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 100px;">
                            <p>
                                <a class="dark-grey-text" href="#!">Sua Conta</a>
                            </p>
                            <p>
                                <a class="dark-grey-text" href="#!">Dúvidas</a>
                            </p>
                            <p>
                                <a class="dark-grey-text" href="#!">Sugestões</a>
                            </p>
                            <!-- <p>
                                <a class="dark-grey-text" href="#!">Help</a>
                            </p> -->

                        </div>
                        <!-- Grid column -->

                        <!-- Grid column -->
                        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">

                            <!-- Links -->
                            <h6 class="text-uppercase font-weight-bold">Contato</h6>
                            <hr class="teal accent-3 mb-4 mt-0 d-inline-block mx-auto" style="width: 100px;">
                            <p>Sorocaba, SP, Brasil</p>
                            <p>(15) 98131-9363</p>

                        </div>
                        <!-- Grid column -->

                    </div>
                    <!-- Grid row -->

                </div>
                <!-- Footer Links -->

                <!-- Copyright -->
                <div class="footer-copyright text-center text-black-50 py-3" id="copyright-saladonna">© 2018 Copyright:
                    <a href="#"> Saladonna</a>
                </div>
                <!-- Copyright -->

            </footer>
            <!-- Footer -->
        </div>

        <script src="js/jquery-3.3.1.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/mdb.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/main.js"></script>
        <script src="js/shopping-cart.js"></script>

        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>                                             <!-- AOS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>                <!-- Toastr -->
        <script src="lib/jquery-mask/jquery.mask.js"></script>                                                      <!-- JQuery Mask -->
        
    </body>
</html>