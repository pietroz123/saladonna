-- Saladas
INSERT INTO saladas (nome) VALUES ('Donna Caprese');
INSERT INTO saladas (nome) VALUES ('Donna Suprema');
INSERT INTO saladas (nome) VALUES ('Donna Detox');
INSERT INTO saladas (nome) VALUES ('Donna Fitness');
INSERT INTO saladas (nome) VALUES ('Donna Tropical');
INSERT INTO saladas (nome) VALUES ('Salada de Frutas');

-- Ingredientes
INSERT INTO ingredientes (nome) VALUES ('Macarrão integral');
INSERT INTO ingredientes (nome) VALUES ('Manjericão');
INSERT INTO ingredientes (nome) VALUES ('Queijo branco');
INSERT INTO ingredientes (nome) VALUES ('Tomate cereja');
INSERT INTO ingredientes (nome) VALUES ('Mix de folhas');
INSERT INTO ingredientes (nome) VALUES ('Milho verde');
INSERT INTO ingredientes (nome) VALUES ('Chia em grãos');
INSERT INTO ingredientes (nome) VALUES ('Grão de bico');
INSERT INTO ingredientes (nome) VALUES ('Pepino');
INSERT INTO ingredientes (nome) VALUES ('Frango grelhado');
INSERT INTO ingredientes (nome) VALUES ('Beterraba');
INSERT INTO ingredientes (nome) VALUES ('Cenoura');
INSERT INTO ingredientes (nome) VALUES ('Couve');
INSERT INTO ingredientes (nome) VALUES ('Vagem');
INSERT INTO ingredientes (nome) VALUES ('Brócolis');
INSERT INTO ingredientes (nome) VALUES ('Tomate seco');
INSERT INTO ingredientes (nome) VALUES ('Batata doce');
INSERT INTO ingredientes (nome) VALUES ('Ovo de codorna');
INSERT INTO ingredientes (nome) VALUES ('Palmito');
INSERT INTO ingredientes (nome) VALUES ('Peito de peru');
INSERT INTO ingredientes (nome) VALUES ('Morango');
INSERT INTO ingredientes (nome) VALUES ('Abacaxi');
INSERT INTO ingredientes (nome) VALUES ('Kiwi');
INSERT INTO ingredientes (nome) VALUES ('Mamão');
INSERT INTO ingredientes (nome) VALUES ('Melão');
INSERT INTO ingredientes (nome) VALUES ('Banana');


-- Salada_Ingredientes
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (1, 1);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (1, 2);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (1, 3);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (1, 4);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (1, 5);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (1, 6);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (1, 7);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (2, 8);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (2, 9);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (2, 10);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (2, 11);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (2, 5);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (2, 6);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (2, 12);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (3, 13);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (3, 10);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (3, 12);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (3, 14);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (3, 15);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (3, 16);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (3, 5);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (4, 4);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (4, 12);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (4, 11);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (4, 17);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (4, 18);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (4, 5);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (4, 6);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (5, 4);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (5, 12);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (5, 19);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (5, 20);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (5, 21);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (5, 5);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (5, 7);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (6, 21);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (6, 22);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (6, 23);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (6, 24);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (6, 25);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (6, 26);
INSERT INTO salada_ingredientes (id_salada, id_ingrediente) VALUES (6, 27);


-- Selecionar todas as saladas e seus ingredientes
SELECT * FROM ingredientes AS i, saladas AS s, salada_ingredientes AS si 
WHERE si.id_salada = s.id
AND si.id_ingrediente = i.id;


-- Molhos
INSERT INTO molhos (nome) VALUES ('Italiano');
INSERT INTO molhos (nome) VALUES ('Rosé');
INSERT INTO molhos (nome) VALUES ('Caseiro');