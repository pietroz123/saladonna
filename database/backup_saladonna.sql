-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 16-Jan-2019 às 02:00
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saladonna`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `ingredientes`
--

CREATE TABLE `ingredientes` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `imagem` varchar(50) NOT NULL DEFAULT 'loading.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `ingredientes`
--

INSERT INTO `ingredientes` (`id`, `nome`, `imagem`) VALUES
(1, 'Macarrão integral', 'macarrao.jpg'),
(2, 'Manjericão', 'manjericao.jpg'),
(3, 'Queijo branco', 'queijobranco.jpg'),
(4, 'Tomate cereja', 'tomatecereja.jpg'),
(5, 'Mix de folhas', 'alface.jpg'),
(6, 'Milho verde', 'milho3.png'),
(7, 'Chia em grãos', 'chia.png'),
(8, 'Grão de bico', 'graobico.png'),
(9, 'Pepino', 'pepino.png'),
(10, 'Frango grelhado', 'frango.png'),
(11, 'Beterraba', 'beterraba2.jpg'),
(12, 'Cenoura', 'cenoura.jpg'),
(13, 'Couve', 'couve.jpg'),
(14, 'Vagem', 'vagem.jpg'),
(15, 'Brócolis', 'brocolis.png'),
(16, 'Tomate seco', 'tomateseco.jpg'),
(17, 'Batata doce', 'batatadoce.png'),
(18, 'Ovo de codorna', 'codorna2.png'),
(19, 'Palmito', 'palmito.jpg'),
(20, 'Peito de peru', 'peitoperu.png'),
(21, 'Manga', 'manga.png'),
(22, 'Morango', 'morango.png'),
(23, 'Abacaxi', 'abacaxi.jpg'),
(24, 'Kiwi', 'kiwi.png'),
(25, 'Mamão', 'mamao.jpg'),
(26, 'Melão', 'melao.png'),
(27, 'Banana', 'banana2.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `molhos`
--

CREATE TABLE `molhos` (
  `id` int(11) NOT NULL,
  `nome` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `molhos`
--

INSERT INTO `molhos` (`id`, `nome`) VALUES
(1, 'Italiano'),
(2, 'Rosé'),
(3, 'Caseiro');

-- --------------------------------------------------------

--
-- Estrutura da tabela `saladas`
--

CREATE TABLE `saladas` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `imagem` varchar(50) NOT NULL DEFAULT 'loading.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `saladas`
--

INSERT INTO `saladas` (`id`, `nome`, `descricao`, `imagem`) VALUES
(1, 'Donna Caprese', 'Perfeita para quem quer uma vida saudável', 'donna_caprese.png'),
(2, 'Donna Suprema', 'Perfeita para quem quer uma vida saudável', 'donna_suprema.png'),
(3, 'Donna Detox', 'Perfeita para quem quer uma vida saudável', 'donna_detox.png'),
(4, 'Donna Fitness', 'Perfeita para quem quer uma vida saudável', 'donna_fitness.png'),
(5, 'Donna Tropical', 'Perfeita para quem quer uma vida saudável', 'donna_tropical.jpg'),
(6, 'Salada de Frutas', 'Perfeita para quem quer uma vida saudável', 'salada_frutas.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `salada_ingredientes`
--

CREATE TABLE `salada_ingredientes` (
  `id_salada` int(11) NOT NULL,
  `id_ingrediente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `salada_ingredientes`
--

INSERT INTO `salada_ingredientes` (`id_salada`, `id_ingrediente`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(2, 5),
(2, 6),
(2, 8),
(2, 9),
(2, 10),
(2, 11),
(2, 12),
(3, 5),
(3, 10),
(3, 12),
(3, 13),
(3, 14),
(3, 15),
(3, 16),
(4, 4),
(4, 5),
(4, 6),
(4, 11),
(4, 12),
(4, 17),
(4, 18),
(5, 4),
(5, 5),
(5, 7),
(5, 12),
(5, 19),
(5, 20),
(5, 21),
(6, 21),
(6, 22),
(6, 23),
(6, 24),
(6, 25),
(6, 26),
(6, 27);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ingredientes`
--
ALTER TABLE `ingredientes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nome` (`nome`);

--
-- Indexes for table `molhos`
--
ALTER TABLE `molhos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saladas`
--
ALTER TABLE `saladas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nome` (`nome`);

--
-- Indexes for table `salada_ingredientes`
--
ALTER TABLE `salada_ingredientes`
  ADD PRIMARY KEY (`id_salada`,`id_ingrediente`),
  ADD KEY `Saladonna_Ingredientes_fk1` (`id_ingrediente`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ingredientes`
--
ALTER TABLE `ingredientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `molhos`
--
ALTER TABLE `molhos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `saladas`
--
ALTER TABLE `saladas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `salada_ingredientes`
--
ALTER TABLE `salada_ingredientes`
  ADD CONSTRAINT `Saladonna_Ingredientes_fk0` FOREIGN KEY (`id_salada`) REFERENCES `saladas` (`id`),
  ADD CONSTRAINT `Saladonna_Ingredientes_fk1` FOREIGN KEY (`id_ingrediente`) REFERENCES `ingredientes` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
