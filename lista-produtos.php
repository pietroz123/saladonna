<?php 
    include $_SERVER['DOCUMENT_ROOT'].'/cabecalho.php'; 
    require_once $_SERVER['DOCUMENT_ROOT'].'/database/conexao.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/config/sessao.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/includes/funcoes.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/includes/funcoes-carrinho.php';


    $saladas = listar_query($conexao, "SELECT * FROM saladas;");

?>

<h1 class="titulo-pagina">Nossas Saladas</h1>

    <!-- Modal para detalhes das saladas -->
    <div class="modal" id="modal-detalhes-salada">
        <div class="modal-dialog" id="detalhes-salada">
            <!-- Preenchido a partir de AJAX -->
        </div>
    </div>

    <!-- Cria todos os cartões para as saladas -->
    <div class="produtos">
    <?php
        $i = 0;
        foreach ($saladas as $salada) {
            $i = $i + 1;
    ?>
        <div class="produto-item z-depth-2" id="item<?= $i ?>" data-aos="zoom-in">
            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="img/saladas/<?= $salada['imagem'] ?>" alt="Imagem <?= $salada['nome'] ?>">
                <div class="card-body">
                    <h5 class="card-title"><?= $salada['nome'] ?></h5>
                    <div class="container">
                        <button type="submit" class="btn btn-info btn-block botao-pequeno btn-detalhes" id="<?= $salada['id'] ?>">Adicionar ao carrinho</button>
                    </div>
                </div>
            </div>
        </div>
    <?php
        }
    ?>
    </div>

    <input id="quantidade-itens" type="hidden" value="<?= $i ?>">
    

<?php
    include $_SERVER['DOCUMENT_ROOT'].'/rodape.php';
?>

<script>
    // Inicializa o plugin para dar zoom nos cartões
    AOS.init();
    
    var quantidade = document.getElementById("quantidade-itens").value;
    var delayCount = 50;
    var itemCount = 1;
    
    while (itemCount <= quantidade) {
        var item = document.getElementById("item" + itemCount);
        var delay = document.createAttribute("data-aos-delay");
        delay.value = delayCount;
        item.attributes.setNamedItem(delay);
        
        delayCount += 50;
        itemCount++;
    }
</script>