<?php
    ob_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Saladonna</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="logo.png" type="image/x-icon">

        <!-- CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/mdb.css">
        <link rel="stylesheet" href="css/saladonna.css">
        <link rel="stylesheet" href="css/shopping-cart.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.css">            <!-- Toastr -->

        <!-- Plugins -->
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    </head>

    <body>

        <header id="header">
            <nav class="navbar navbar-expand">
            
                <div class="container py-2">
                    <div class="navbar-header">
                        <a href="index.php" class="navbar-brand">Home</a>
                    </div>
                    
                    <!-- Barra de navegação esquerda -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a href="lista-produtos.php" class="nav-link">Produtos</a>
                        </li>
                        <li class="nav-item">
                            <a href="contato.php" class="nav-link">Contato</a>
                        </li>
                    </ul>
                    
                    <!-- Barra de navegação direita: carrinho -->
                    <ul class="navbar-nav">
                        <li class="nav-item" id="nav-item-carrinho">
                            <!-- ... -->
                            <?php include $_SERVER['DOCUMENT_ROOT'].'/scripts/adiciona-carrinho.php'; ?>
                        </li>
                    </ul>
                </div>
                
            </nav>
            

        </header>
            
        <main class="container" id="content">
            <div class="conteudo-principal">