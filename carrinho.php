<?php 
    include $_SERVER['DOCUMENT_ROOT'].'/cabecalho.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/includes/logica-produtos.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/config/sessao.php';
?>

    <h1 class="titulo-pagina">Seu Carrinho</h1>

    <?php
        if (isset($_SESSION['carrinho']) && count($_SESSION['carrinho']) > 0) {
    ?>

    <div class="cartao-carrinho my-5 z-depth-2">

        <header class="carrinho-header py-4">
            <div class="container">
                <div class="row">
                    <div class="col-4">PRODUTO</div>
                    <div class="col">QUANTIDADE</div>
                    <div class="col">PREÇO UNITÁRIO</div>
                    <div class="col">SUBTOTAL <a id="btn-limpar-carrinho"><i class="fa fa-times float-right text-danger" aria-hidden="true"></i></a></div>
                </div>
            </div>
        </header>

        <main class="carrinho-body bg-white py-4">
            <div class="container">
            <?php
                foreach ($_SESSION['carrinho'] as $item_carrinho) {
            ?>
                <div class="row py-2">
                    <div class="col-4">
                        <img src="img/saladas/<?= $item_carrinho['imagem'] ?>" class="img-6rem img-thumbnail float-left ml-3 produto-carrinho-imagem">
                        <p class="text-uppercase produto-carrinho-nome float-left ml-4"><?= $item_carrinho['nome'] ?></p>
                    </div>
                    <div class="col d-flex flex-column">
                        <div id="300" class="mb-2">
                            <span for="qtd-300">300ml</span>
                            <input type="number" class="produto-qtd" value="<?= $item_carrinho['qtd_300'] ?>" min="0" max="10" placeholder="Qtd 300ml" id="qtd-300">
                        </div>
                        <div id="500">
                            <span for="qtd-500">500ml</span>
                            <input type="number" class="produto-qtd" value="<?= $item_carrinho['qtd_500'] ?>" min="0" max="10" placeholder="Qtd 500ml" id="qtd-500">
                        </div>
                    </div>
                    <div class="col">
                        <div class="preco-300 mb-2">R$ <?= PRECO_300 ?></div>
                        <div class="preco-500">R$ <?= PRECO_500 ?></div>
                    </div>
                    <div class="col">
                        <div class="preco-total">R$ <?= preco_item($item_carrinho) ?></div>
                    </div>
                </div>
            <?php
                }
            ?>
            </div>
        </main>

    </div>

    <div class="container">

        <!-- endereco-subtotal -->
        <div class="row text-left" id="endereco-subtotal">
        
            <div class="col-12 col-sm-8 col-md-9 verde-escuro-bg white-text pt-3 z-depth-2" id="cartao-endereco">
            
                <header>
                    <div class="container">
                        <div class="row">
                            <div class="col-4">
                                <span class="text-uppercase cep-text"><i class="fas fa-2x fa-home"></i> digite seu cep</span>
                            </div>
                            <div class="col">
                                <div class="form-inline float-right">
                                    <input type="text" name="cep-compra" id="cep-compra" placeholder="Digite seu CEP aqui" class="form-control">
                                    <button class="btn laranja-bg botao-pequeno ml-2">aplicar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
            
                <main id="opcoes-entrega" class="text-left">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="frete-titulo">Método de Entrega</div>
                                <ul class="frete-opcoes">
                                    <li class="opcao"><label for="retirar-loja"><input type="radio"> Retirar na loja (não disponível)</label></li>
                                    <li class="opcao"><label for="retirar-loja"><input type="radio"> Entregar</label></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </main>
            
            </div>
            <!-- endereco-subtotal -->
            
            <!-- cartao-subtotal -->
            <div class="col verde-mais-claro-bg ml-0 ml-sm-2 z-depth-2" id="cartao-subtotal">
            
                <section class="container painel-subtotal">
                    
                    <ul>
                        <li>
                            Subtotal:
                            <span class="valor">R$ <?= preco_total(); ?></span>
                        </li>
                        <li>
                            Frete:
                            <span class="valor">a definir</span>
                        </li>
                    </ul>
                    
                    <div class="subtotal">
                        <label>Total:</label>
                        <h5 class="subtotal-valor"><b>R$ <?= preco_total(); ?></b></h5>
                    </div>


        
                </section>

                <footer>
                    <button type="button" class="btn botao-pequeno verde-claro-bg btn-block">comprar</button>
                </footer>
            
            </div>
            <!-- cartao-subtotal -->
        
        </div>

    </div>
    <!-- div.container -->

    <?php
        }

        // =======================================================
        // O carrinho está vazio
        // =======================================================
        else {
    ?>
            <div class="alert alert-danger" role="alert">
                <strong>Ops! Seu carrinho está vazio.</strong>
            </div>
    <?php
        }
    ?>

<?php
    include $_SERVER['DOCUMENT_ROOT'].'/rodape.php';
?>

<script>
    $(document).ready(function() {

        // Aplica máscara no CEP
        $('#cep-compra').mask('00000-000');
    
    });
</script>